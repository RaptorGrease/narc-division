﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemOrb : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "foreground";
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder += 10;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
