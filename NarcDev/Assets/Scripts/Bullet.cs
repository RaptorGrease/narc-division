﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Bullet : MonoBehaviour
{
    GameObject explosionPrefab;
    private GunTypes type;
    private GunPrototype proto;
    private int damage = 10;
    private float acceleration = 1.8f;
    public float speed = 5f;
    private float range = 1f;
    public bool spread = false;
    private bool homing = false;
    private int count = 5;
    private float spreadArea = 90;
    private float rotatingSpeed = 200;
    private bool random;
    private EnemyManager emger;
    private Transform nearest;
    private GameObject player;
    Rigidbody2D rb;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        emger = GameObject.Find("enemies").GetComponent<EnemyManager>();
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        if (emger.transform.childCount > 0)
        {
            nearest = emger.transform.GetChild(0);

            foreach (Transform item in emger.transform)
            {
                if (Vector2.Distance(item.position, transform.position) < Vector2.Distance(nearest.position, transform.position))
                {
                    nearest = item;
                }
            }
        }

        if (spread)
        {
            float slice = spreadArea / (count - 1);
            for (var i = 1; i <= count; i++)
            {
                GameObject bullet = Instantiate(gameObject, transform.position, transform.rotation);
                bullet.GetComponent<Bullet>().Init(proto);
                bullet.GetComponent<Bullet>().speed += Random.Range(-0.2f, 0.2f);
                bullet.GetComponent<Bullet>().spread = false;
                bullet.GetComponent<Bullet>().enabled = true;
                bullet.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder;
                if (type == GunTypes.rocket)
                {
                    if (i % 2 == 0)
                    {
                        double x = i - 0.9 * i;
                        float x1 = Convert.ToSingle(x);
                        bullet.transform.Translate(x1 + Random.Range(-0.2f, 0.2f), -x1 + Random.Range(-0.2f, 0.2f), 0);
                    }
                    else
                    {
                        double x = -i + 0.9 * i;
                        float x1 = Convert.ToSingle(x);
                        bullet.transform.Translate(x1 + Random.Range(-0.2f, 0.2f), x1 + Random.Range(-0.2f, 0.2f), 0);
                    }
                }
                else
                {
                    bullet.transform.Rotate(0, 0, (spreadArea * 0.5f) - slice * (i - 1));
                }
                switch (type)
                {
                    case GunTypes.stream:
                        Destroy(bullet, bullet.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length);
                        break;
                    default:
                        Destroy(bullet, (10 / speed) * range);
                        break;
                }
            }
        }
    }

    // for testing effects of "random"
    private void FixedUpdate()
    {
        if (random)
        {
            GetComponent<SpriteRenderer>().color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), 1f);
            transform.localScale = new Vector3(Random.Range(0, 1f), Random.Range(0, 1f), 1);
        }
    }

    // for performing bullet behaviour
    private void Update()
    {
        if (Vector2.Distance(transform.position, GameObject.FindGameObjectWithTag("Player").gameObject.transform.position) > 7)
        {
            Destroy(gameObject);
        }
        if (NextTarget() && homing)
        {
            Vector2 point2Target = transform.position - nearest.transform.position;
            point2Target.Normalize();
            float value = Vector3.Cross(point2Target, transform.up).z;
            rb.angularVelocity = rotatingSpeed * value;
        }

        switch (type)
        {
            case GunTypes.projectile:
                rb.velocity = transform.up * speed;
                break;
            case GunTypes.stream:
                transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
                break;
            case GunTypes.rocket:
                speed += ((speed) * acceleration * acceleration * Time.deltaTime);
                rb.velocity = transform.up * speed;
                break;
        }

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyHealth>().TakeDamage(damage);
            if (type != GunTypes.stream)
            {
                if (proto.explosionType != ExplosionTypes.none)
                {
                    GameObject explo = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
                    explo.GetComponent<PlayerExplosion>().type = proto.explosionType;
                    explo.GetComponent<PlayerExplosion>().filter = proto.explosionColorFilter;
                    explo.GetComponent<PlayerExplosion>().enabled = true;
                }
                Destroy(gameObject);
            }
        }
        if (collision.gameObject.tag == "Boss")
        {
            if (type != GunTypes.stream)
            {
                collision.gameObject.GetComponent<BossHealth>().TakeDamage(damage);
                if (proto.explosionType != ExplosionTypes.none)
                {
                    GameObject explo = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
                    explo.GetComponent<PlayerExplosion>().type = proto.explosionType;
                    explo.GetComponent<PlayerExplosion>().filter = proto.explosionColorFilter;
                    explo.GetComponent<PlayerExplosion>().enabled = true;
                }

                Destroy(gameObject);
            }
        }

        if (collision.gameObject.tag != "Player" && collision.gameObject.Equals(this))
        {
            Destroy(gameObject);
        }
    }

    // GunManager uses this to give all settings
    public void Init(GunPrototype gp)
    {
        explosionPrefab = gp.explosionPrefab;
        proto = gp;
        type = gp.type;
        damage = gp.damage;
        speed = gp.speed;
        range = gp.range;
        spread = gp.spread;
        homing = gp.homing;
        count = gp.extraBullets;
        spreadArea = gp.spreadArea;
        rotatingSpeed = gp.rotatingSpeed;
        random = gp.random;
        enabled = true;
    }

    // find the nearest target for homing bullets
    bool NextTarget()
    {
        nearest = (GameObject.FindObjectOfType<Boss>()) ? GameObject.FindObjectOfType<Boss>().transform : null;
       
        foreach (EnemyAI item in GameObject.FindObjectsOfType<EnemyAI>())
        {
            if (item.gameObject.activeInHierarchy)
            {
                nearest = item.transform;
                break;
            }
        }

        if (nearest)
        {
            foreach (EnemyAI item in GameObject.FindObjectsOfType<EnemyAI>())
            {
                if (item.gameObject.activeInHierarchy)
                {
                    if (Vector2.Distance(item.transform.position, transform.position) < Vector2.Distance(nearest.position, transform.position))
                    {
                        nearest = item.transform;
                    }
                }
            }

            foreach (Boss item in GameObject.FindObjectsOfType<Boss>())
            {
                if (item.gameObject.activeInHierarchy)
                {
                    if (Vector2.Distance(item.transform.position, transform.position) < Vector2.Distance(nearest.position, transform.position))
                    {
                        nearest = item.transform;
                    }
                }
            }

            if (Vector2.Distance(nearest.position, player.transform.position) < 4f)
            {
                return true;
            }
        }
        return false;
    }

}