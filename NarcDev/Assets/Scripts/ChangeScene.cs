﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;

using UnityEngine.SceneManagement;







public class ChangeScene : MonoBehaviour

{

    public GameObject canvas;

    public GameObject transition;

    GameObject loadingAnimation;

    public float delay = 1.5f;

    bool loading = false;



    private void Start()

    {

        canvas = GameObject.Find("canvas");

       

    }



    private void Update()

    {

        if (loading)

        {
            loadingAnimation.transform.position = GameObject.FindGameObjectWithTag("Player").transform.position;
            GameObject.Find("canvas").SetActive(false);

            delay -= Time.deltaTime;

            if (delay < 0)

            {

                int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

                SceneManager.LoadScene(nextSceneIndex);

                Destroy(loadingAnimation, 1.5f);

            }

        }

    }



    void OnTriggerEnter2D(Collider2D other)

    {

        if (other.gameObject.CompareTag("Player"))

        {

            loadingAnimation = Instantiate(transition, transform.position, transform.rotation);

            loadingAnimation.transform.parent = other.transform;

            loading = true;

        }

    }

}

