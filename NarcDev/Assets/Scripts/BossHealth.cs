﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHealth : MonoBehaviour
{

    public int maxHealth = 30;
    public int currentHealth;
    public EnemySpawner[] es;
    public GameObject go;


    void Start()
    {
        go = GameObject.Find("skenevaihtotriggeriboksi");
        go.SetActive(false);
        currentHealth = maxHealth;
        es = GameObject.FindObjectsOfType<EnemySpawner>();
        foreach (EnemySpawner item in es)
        {
            item.gameObject.SetActive(false);
        }

    }

    private void Update()
    {
        for (int i = 0; i < es.Length; i++)
        {
            es[i].gameObject.SetActive(currentHealth < (maxHealth * (0.75f - i * (0.75f / es.Length))));
        }
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            Dead();
        }
    }

    void Dead()
    {
        go.SetActive(true);
        Destroy(gameObject, 1f);
    }


}
