﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

	public GameObject dBox;
	public Text lines;
	public bool dialogActive;
	public string[] dialogLines;
	public int currentLine = 0;
	public Button next;

	void Start()
	{
		next.GetComponent<Button>().onClick.AddListener(NextLine);
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void ShowBox(string dialogue)
	{
		dialogActive = true;
		dBox.SetActive(true);
		lines.text = dialogue;
	}

	public void ShowDialog()
	{
		dialogActive = true;
		dBox.SetActive(true);
		lines.text = dialogLines[currentLine];
	}

	void NextLine()
	{
			/* dBox.SetActive(false);
            dialogActive = false;*/
			
		currentLine++;
		if (currentLine >= dialogLines.Length)
		{
			dBox.SetActive(false);	
			dialogActive = false;
			currentLine = 0;
			Time.timeScale = 1;
			return;
		}
		lines.text = dialogLines[currentLine];
		
	}
}
