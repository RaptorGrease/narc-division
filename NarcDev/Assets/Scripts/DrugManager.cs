﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum DrugTypes { weed, cocaine, meth, shrooms };

public class DrugManager : MonoBehaviour
{
    GameObject player;
    GameObject miniMenu;
    public GameObject btn;
    public List<Button> buttons;
    public bool isPressed = false;
    public DrugTypes selection = DrugTypes.weed;
    public float timer;

    void Start()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        player = GameObject.FindGameObjectWithTag("Player");
        DrugTypes index = DrugTypes.weed;

        // get all child objects
        foreach (Transform item in transform)
        {
            // check if current child is MiniMenu
            if (item.gameObject.name == "MiniMenu")
            {
                miniMenu = item.gameObject;
            }

            // check if current child has Button (should be the main button)
            if (item.gameObject.GetComponent<Button>())
            {
                btn = item.gameObject;
            }

            // get all (grand) child objects
            foreach (Transform obj in item)
            {
                // check if one of the (grand) children is a Button (should be MiniMenu button)
                if (obj.gameObject.GetComponent<Button>())
                {
                    obj.gameObject.AddComponent<EventTrigger>();
                    entry = new EventTrigger.Entry();
                    entry.eventID = EventTriggerType.PointerClick;
                    // delegate PointerClick to MouseClick
                    DrugTypes i = index++;
                    entry.callback.AddListener(delegate { MouseClick(i); });
                    obj.gameObject.GetComponent<EventTrigger>().triggers.Add(entry);
                    buttons.Add(obj.gameObject.GetComponent<Button>());
                }
            }
        }

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        // delegate PointerDown to MouseDown
        entry.callback.AddListener((data) => { MouseDown((PointerEventData)data); });
        btn.GetComponent<EventTrigger>().triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        // delegate PointerUp to MouseUp
        entry.callback.AddListener((data) => { MouseUp((PointerEventData)data); });
        btn.GetComponent<EventTrigger>().triggers.Add(entry);

        MouseClick(selection);
    }

    public void MouseClick(DrugTypes index)
    {
        selection = index;
        CloseMiniMenu();
    }

    private void MouseUp(PointerEventData data)
    {
        isPressed = false;
        if (timer > 0)
        {
            ButtonClick();
        }
    }

    private void MouseDown(PointerEventData data)
    {
        isPressed = true;
        timer = 1f;
    }

    void FixedUpdate()
    {
        if (isPressed)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                isPressed = false;
                OpenMiniMenu();
            }
        }
    }

    void OpenMiniMenu()
    {
        btn.gameObject.SetActive(false);
        miniMenu.SetActive(true);
    }

    void CloseMiniMenu()
    {
        btn.gameObject.SetActive(true);
        miniMenu.SetActive(false);
        btn.GetComponentInChildren<Text>().text = buttons[(int) selection].GetComponentInChildren<Text>().text;
    }

    void ButtonClick()
    {
        print(string.Format("You took some {0}. Feels good, man.", btn.GetComponentInChildren<Text>().text));
        player.SendMessage("TakeDrugs", selection);
    }
}
