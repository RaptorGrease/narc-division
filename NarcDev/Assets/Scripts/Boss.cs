﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using UnityEngine.AI;

public class Boss : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject orbPrefab;
    public float fireRate = 15f;
    public float moveSpeed = 0.01f;
    public float _senseRange = 5f;
    public float _shootRange;
    public float speed = 5f;
    public int cooldown = 30;
    private int counter = 0;
    public int count = 5;
    public float spreadArea = 90;
    private Animator _anim;
    private GameObject player;
    private Health _playerHealth;
    private BossHealth _enemyHealth;
    private bool _isAlive = true;
    private bool _isInRange = false;
    private bool _isAiming = false;
    private int _dir = 8;
    private float delay = 1f;
    private float _angle;
    private OrbSpell orb;
    Vector2 setDir;
    private BossDialog bd;
    
    void Start()
    {
        bd = GetComponent<BossDialog>();
        _shootRange = _senseRange * 0.75f;
        player = GameObject.FindGameObjectWithTag("Player");
        _playerHealth = player.GetComponent<Health>();
        _enemyHealth = GetComponent<BossHealth>();
        orb = GetComponent<OrbSpell>();
    }
    void Awake()
    {
        _isAiming = false;

    }

    void FixedUpdate()
    {
        if (bd.encounterDone)
        {

        if (_isAlive && Time.timeScale == 1 && _playerHealth.currentHealth > 0)
        {
           
                    counter++;
                    if (!_isAiming)
                    {
                        Vector2 position = transform.position;
                        position += setDir * Time.deltaTime;
                        // transform.position = position;
                        delay -= Time.deltaTime;
                        if (delay < 0)
                        {
                            delay = 1f;
                            Fire();
                        }
                        _isAiming = false;
                        // transform.Translate(new Vector3((player.transform.position.x), (player.transform.position.y), 0) * moveSpeed, Space.World);

                    }
                }
            }
        }
 

    void Update()
    {


        _isAlive = _enemyHealth.currentHealth > 0;
        _isInRange = Vector2.Distance(transform.position, player.transform.position) <= (_shootRange - 0.1f);

        _angle = Mathf.Atan2(player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * 180 / Mathf.PI;



    }
    public void SetDirection(Vector2 direction)
    {
        setDir = direction.normalized;
        _isAiming = true;
    }
    void Fire()
    {


        Vector3 spawnLoc = transform.position;
        spawnLoc.y -= 0.3f;
        GameObject orb = Instantiate(orbPrefab, spawnLoc, Quaternion.Euler(0, 0, _angle - 86));


        orb.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;

    }
}