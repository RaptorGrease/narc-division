﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skeneEnter : MonoBehaviour {
    public GameObject transitionOnEnter;
    public GameObject canvas;
    float delay = 1.5f;
    GameObject loadingAnimation;
    private bool isTriggered = false;

    private void Start()
    {
        canvas = GameObject.Find("canvas");
        canvas.SetActive(false);
        

    }
    private void Update()
    {
        delay -= Time.deltaTime;
        if (delay < 0)
        {
            canvas.SetActive(true);
            isTriggered = true;
        }
    }

    void OnTriggerEnter2D(Collider2D skeneEnter)
    {
        if (!isTriggered)
        {
            if (skeneEnter.gameObject.CompareTag("Player"))
            {
                loadingAnimation = Instantiate(transitionOnEnter, transform.position, transform.rotation);
                loadingAnimation.transform.parent = gameObject.transform;
                Destroy(loadingAnimation, 1.3f);
                isTriggered = true;
                
                
            }
        }

    }
}
