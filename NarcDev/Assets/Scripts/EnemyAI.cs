﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class EnemyAI : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float fireRate = 15f;
    public float moveSpeed = 0.01f;
    public float senseRange = 5f;
    public float shootRange;
    public float cooldown = 0.5f;
    public bool canMove = true;
    public bool canShoot = true;
    public bool isReady = false;

    private Animator anim;
    private GameObject player;
    private Vector3 wayPoint;
    private Health playerHealth;
    private EnemyHealth enemyHealth;
    private bool isAlive = true;
    private bool isDetect = false;
    private bool isInRange = false;
    private bool isAiming = false;
    private bool isFire = false;
    private int dir = 8;
    private float delay = 0;
    private float angle;

    void Start()
    {
        delay = cooldown;
        shootRange = senseRange * 0.75f;
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<Health>();
        enemyHealth = GetComponent<EnemyHealth>();
    }

    void FixedUpdate()
    {
        if (isAlive && Time.timeScale == 1 && playerHealth.currentHealth > 0 && isReady)
        {
            if (canMove && !isFire)
            {
                if (isDetect && !isInRange && !isAiming)
                {
                    transform.Translate(new Vector3((player.transform.position.x - transform.position.x), (player.transform.position.y - transform.position.y), 0) * moveSpeed, Space.World);
                }
            }

            if (canShoot && !isFire)
            {
                if (canMove && isDetect)
                {
                    anim.SetInteger("direction", dir);
                }

                if (isInRange)
                {
                    isAiming = true;
                }
            }

            if (isAiming)
            {
                delay -= Time.deltaTime;
                if (delay < 0)
                {
                    delay = cooldown;
                    isFire = true;
                    isAiming = false;
                }
            }

            if (isFire)
            {
                if (isInRange)
                {
                    delay -= Time.deltaTime;
                    if (delay < cooldown * 0.5f)
                    {
                        delay = cooldown;
                        isFire = false;
                        Fire();
                    }
                }
                else
                {
                    delay = cooldown;
                    isFire = false;
                }
            }
        }
    }

    void Update()
    {
        anim.SetBool("isShoot", isFire);
        anim.SetBool("isWalk", !isFire && !isAiming && !isInRange && isDetect);

        isAlive = enemyHealth.currentHealth > 0;
        isDetect = Vector2.Distance(transform.position, player.transform.position) < senseRange;
        isInRange = Vector2.Distance(transform.position, player.transform.position) <= (shootRange - 0.1f);

        angle = Mathf.Atan2(player.transform.position.y - transform.position.y, player.transform.position.x - transform.position.x) * 180 / Mathf.PI;
        int suunta = 5;
        suunta = (angle > 157.50f && angle < 180.00f) ? 4 : suunta;
        suunta = (angle > 112.50f && angle < 157.50f) ? 7 : suunta;
        suunta = (angle > 67.500f && angle < 112.50f) ? 8 : suunta;
        suunta = (angle > 22.500f && angle < 67.500f) ? 9 : suunta;
        suunta = (angle > -22.50f && angle < 22.500f) ? 6 : suunta;
        suunta = (angle > -67.50f && angle < -22.50f) ? 3 : suunta;
        suunta = (angle > -112.5f && angle < -67.50f) ? 2 : suunta;
        suunta = (angle > -157.5f && angle < -112.5f) ? 1 : suunta;
        suunta = (angle > -180.0f && angle < -157.5f) ? 4 : suunta;
        if (dir != suunta)
        {
            dir = suunta;
            anim.SetBool("isTurn", true);
        }
        else
        {
            anim.SetBool("isTurn", false);
        }
    }

    void Fire()
    {
        bool isDown = false;
        Vector3 spawnLoc = transform.position;
        switch (dir)
        {
            case 1:
            case 4:
            case 7:
                spawnLoc.x -= 0.2f;
                break;
            case 3:
            case 6:
            case 9:
                spawnLoc.x += 0.2f;
                break;
            case 2:
                isDown = true;
                spawnLoc.x -= 0.1f;
                break;
            case 8:
                spawnLoc.y -= 0.2f;
                break;
        }
        GameObject bullet = Instantiate(bulletPrefab, spawnLoc, Quaternion.Euler(0, 0, angle - 90));
        if (isDown)
        {
            bullet.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
        }
        bullet.GetComponent<Rigidbody2D>().velocity = bullet.transform.up * 5;
        Destroy(bullet, 2.0f);
    }
}
