﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public bool disableMove = false;
    public bool disaleShoot = false;
    public bool respawn = false;
    public GameObject[] enemyPrefabs;
    public List<GameObject> enemies, backup;

    void Start()
    {
        GameObject clones = new GameObject("enemies (Clone)");
        foreach (EnemyAI item in GetComponentsInChildren<EnemyAI>())
        {
            item.isReady = true;
            enemies.Add(item.gameObject);
            backup.Add(Instantiate(item.gameObject));
            int i = backup.Count - 1;
            backup[i].transform.position = item.transform.position;
            backup[i].transform.parent = clones.transform;
            backup[i].SetActive(false);
        }
    }

    void Update()
    {
        if (respawn)
        {
            respawn = false;
            foreach (GameObject item in enemies)
            {
                Destroy(item);
            }
            enemies.Clear();
            foreach (GameObject item in backup)
            {
                enemies.Add(Instantiate(item));
                int i = enemies.Count - 1;
                enemies[i].transform.SetParent(gameObject.transform);
                enemies[i].SetActive(true);
            }
        }

        foreach (GameObject item in enemies)
        {
            if (item)
            {
                item.GetComponent<EnemyAI>().canMove = !disableMove;
                item.GetComponent<EnemyAI>().canShoot = !disaleShoot;
            }
        }
    }

    void EnemyListChanged()
    {
        foreach(EnemyAI item in FindObjectsOfType<EnemyAI>())
        {
            if (!enemies.Contains(item.gameObject))
            {
                item.isReady = true;
                enemies.Add(item.gameObject);
                backup.Add(Instantiate(item.gameObject));
                int i = backup.Count - 1;
                backup[i].transform.position = item.transform.position;
                backup[i].transform.parent = GameObject.Find("enemies (Clone)").transform;
                backup[i].SetActive(false);
            }
        }
    }
}
