﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbSpell : MonoBehaviour
{

    public GameObject bulletPrefab;


    public float spreadArea = 360;
    public int count = 8;
    public int counter = 0;
    private int currentBullet = 0;
    private float delay = 0.2f;
    private float finalBang;
    private GameObject player;
    private float chillCount = 0;
    void Start()
    {
        count = Random.Range(8,11);
        finalBang = Random.Range(2f, 3f);
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        finalBang -= Time.deltaTime;
        delay -= Time.deltaTime;
        if (delay < 0)
        {
            delay = 0.2f;
            ShootOrb();
            currentBullet++;
            if (currentBullet >= count)
            {
                currentBullet = 0;
            }
        }
        if (Vector2.Distance(transform.position, player.transform.position) <= 1f)
        {
            chillCount -= Time.deltaTime;
            if (chillCount < 0)
            {
                chillCount = 1.5f;
            player.SendMessage("ChillOut");
            }
        }
        if (finalBang < 0)
        {
            transform.Rotate(0,0,-22.5f);
            for (currentBullet = 0; currentBullet < count; currentBullet++)
            {
                ShootOrb();
            }
            Destroy(gameObject);
        }
        GetComponent<Rigidbody2D>().velocity = transform.up * 2f;
    }
    public void ShootOrb()
    {

        float slice = spreadArea / count;
            GameObject bullet = Instantiate(bulletPrefab, transform.position, transform.rotation);

            bullet.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
        bullet.transform.Rotate(0, 0, (spreadArea * 0.5f) - slice * (currentBullet));
        Destroy(bullet,3f);
    }

}


