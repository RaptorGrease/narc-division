﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public enum GunTypes
{
    projectile,
    stream,
    rocket
};

[Serializable]
public enum ExplosionTypes
{
    none,
    smallCircle,
    smallMushroom,
    bigCircle,
    bigMushroom,
    nuke,
    dustCloud
};

[Serializable]
public class GunPrototype
{
    public string name;
    public GameObject bulletPrefab;
    public GameObject explosionPrefab;
    public ExplosionTypes explosionType;
    public Color explosionColorFilter = new Color(1f, 1f, 1f, 1f);
    public GunTypes type;
    public int damage;
    public float speed = 5;
    public float shootDelay;
    public int magSize;
    public float range = 1;
    public float reloadTime;
    public bool spread = false;
    public int extraBullets = 5;
    public float spreadArea = 90;
    public bool homing = false;
    public float rotatingSpeed = 200;
    public bool random = false;
    public int shotsLeft;

    private float reloadCooldown;
    private float shootCooldown;

    public float ReloadCooldown
    {
        get
        {
            return reloadCooldown;
        }

        set
        {
            reloadCooldown = value;
        }
    }
    public void Init()
    {

        ReloadCooldown = reloadTime;
        shotsLeft = magSize;
    }

    public void Update()
    {
        shootCooldown -= Time.deltaTime;
        if (shotsLeft < 1)
        {
            ReloadCooldown -= Time.deltaTime;
            if (ReloadCooldown <= 0)
            {
                shotsLeft = magSize;
                ReloadCooldown = reloadTime;
            }
        }
    }

    public bool CanShoot()
    {
        if (shootCooldown < 0)
        {
            shootCooldown = shootDelay;
            return true;
        }
        return false;
    }

    public void RemoveShot()
    {
        shotsLeft--;
    }
}

public class GunManager : MonoBehaviour
{
    public int selectedGun = 0;
    public Sprite borderSprite;
    public GunPrototype[] gunArray;
    Canvas canvas;
    Text ammocounter;
    PlayerAnimation pAnim;
    List<GameObject> buttonList;
    GameObject buttons;

    private void Awake()
    {
        buttons = new GameObject("GunButtons");
        buttonList = new List<GameObject>();
        canvas = FindObjectOfType<Canvas>();

        // define GunButtons rules to automatically place new buttons
        buttons.transform.SetParent(canvas.transform);
        buttons.AddComponent<RectTransform>();
        // make this same size as canvas
        buttons.GetComponent<RectTransform>().sizeDelta = canvas.GetComponent<RectTransform>().sizeDelta;
        // adjust anchor points for easier positioning
        buttons.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        buttons.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        buttons.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        buttons.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        // Layout will place child objects automatically
        buttons.AddComponent<HorizontalLayoutGroup>();
        // buttons are 30x30 with scale 3; spacing of 60 seems to work very well
        buttons.GetComponent<HorizontalLayoutGroup>().spacing = 60;
        // prevent layout from stretching buttons
        buttons.GetComponent<HorizontalLayoutGroup>().childControlWidth = false;
        buttons.GetComponent<HorizontalLayoutGroup>().childControlHeight = false;
        buttons.GetComponent<HorizontalLayoutGroup>().childForceExpandWidth = false;
        buttons.GetComponent<HorizontalLayoutGroup>().childForceExpandHeight = false;

        // make a text object for testing reload related features
        ammocounter = new GameObject("AmmoText").AddComponent<Text>();
        ammocounter.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
        ammocounter.GetComponent<RectTransform>().sizeDelta = new Vector2(115, 40);
        ammocounter.GetComponent<RectTransform>().anchorMin = new Vector2(1, 1);
        ammocounter.GetComponent<RectTransform>().anchorMax = new Vector2(1, 1);
        ammocounter.GetComponent<RectTransform>().pivot = new Vector2(1, 1);
        ammocounter.transform.SetParent(canvas.transform);
    }

    void Start()
    {
        
        if (gunArray.Length > 0)
        {
            foreach (GunPrototype item in gunArray)
            {
                item.Init();
                MakeButton(item);
            }
            ChangeGun(selectedGun);
        }
        pAnim = GetComponent<PlayerAnimation>();
    }

    private void MakeButton(GunPrototype item)
    {
        int n = buttonList.Count;
        GameObject button = new GameObject(string.Format("Button{0}", item.name));
        button.transform.SetParent(buttons.transform);
        // set size to 30x30
        button.AddComponent<RectTransform>().sizeDelta = new Vector2(30, 30);
        // adjust anchor points for easier positioning
        button.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        button.GetComponent<RectTransform>().anchoredPosition = new Vector2(-500 + (n * 90), 0);
        // change scale to 3
        button.GetComponent<RectTransform>().localScale = Vector3.one * 3;
        button.AddComponent<Image>().sprite = item.bulletPrefab.GetComponent<SpriteRenderer>().sprite;
        button.AddComponent<Button>().onClick.AddListener(delegate { ChangeGun(n); });

        GameObject border = new GameObject("Border");
        border.AddComponent<RectTransform>().sizeDelta = new Vector2(30, 30);
        border.transform.SetParent(button.transform);
        border.transform.position = button.transform.position;
        // same as button
        border.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
        border.GetComponent<RectTransform>().localScale = Vector3.one;
        border.AddComponent<Image>().sprite = borderSprite;
        border.GetComponent<Image>().color = new Color(0.72f, 0.71f, 0.43f);

        buttonList.Add(button);
    }

    private void ChangeGun(int i)
    {
        selectedGun = i;
        // deactivate every button border 
        foreach (GameObject item in buttonList)
        {
            item.transform.Find("Border").gameObject.SetActive(false);
        }
        // activate our selection border
        buttonList[i].transform.Find("Border").gameObject.SetActive(true);
    }

    void FixedUpdate()
    {
        gunArray[selectedGun].Update();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "WeaponDrop")
        {
            for (int i = 0; i < gunArray.Length; i++)
            {
                if (other.gameObject.name.Contains(string.Format("gundrop{0}", i + 1)))
                {
                    ChangeGun(i);
                }
            }
            Destroy(other.gameObject);
        }
    }

    public void Shoot()
    {
        GunPrototype gp = gunArray[selectedGun];

        if (gp.CanShoot())
        {
            if (gp.shotsLeft > 0)
            {
                gp.RemoveShot();
                pAnim.isShoot = true;

                GameObject bullet = Instantiate(gp.bulletPrefab, pAnim.spawnLoc, Quaternion.Euler(0, 0, pAnim.angle - 90));
                if (pAnim.isDown)
                {
                    bullet.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                }
                bullet.GetComponent<Bullet>().Init(gp);
                switch (gp.type)
                {
                    case GunTypes.projectile:
                        Destroy(bullet, (10 / gp.speed) * gp.range);
                        break;
                    case GunTypes.stream:
                        bullet.transform.localScale = new Vector3(gp.range, gp.range);
                        Destroy(bullet, bullet.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length);
                        break;
                    case GunTypes.rocket:
                        Destroy(bullet, gp.range);
                        break;
                    default:
                        Destroy(bullet, (10 / gp.speed) * gp.range);
                        break;
                }
            }
        }
    }

    public int GetShots()
    {
        return gunArray[selectedGun].shotsLeft;
    }

    void Update()
    {
        ammocounter.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        ammocounter.text = string.Format("Shots left: {0}\nReloading for: {1:N1}", gunArray[selectedGun].shotsLeft, gunArray[selectedGun].ReloadCooldown);
    }
}
