using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public const int maxHealth = 100;
    public int currentHealth = maxHealth;
    public RectTransform healthBar;
    public Text healthText;
    private float hitTimer = 0.75f;
    private float delay = 3f;
    private bool isDead = false;

    private void Start()
    {
        currentHealth = PlayerPrefs.GetInt("currentHealth");
        if (currentHealth <= 0)
        {
            currentHealth = PlayerPrefs.GetInt("startHealth");
        }
        PlayerPrefs.SetInt("startHealth", currentHealth);
        // healthBar = FindObjectOfType<Canvas>().gameObject.transform.FindChild("Background").GetComponentInChildren<RectTransform>();
        healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
        // healthText = FindObjectOfType<Canvas>().gameObject.transform.FindChild("Background").GetComponentInChildren<Text>();
    }
    private void Update()
    {
        if (isDead)
        {
            delay -= Time.deltaTime;
            if (delay < 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        hitTimer -= Time.deltaTime;
        GetComponent<BoxCollider2D>().enabled = hitTimer < 0;
        healthText.text = "HP: " + currentHealth.ToString();
    }
    public void Heal(int amount)
    {
        currentHealth += amount;
        if (currentHealth >= 100)
        {
            currentHealth = 100;
        }
        PlayerPrefs.SetInt("currentHealth", currentHealth);
        healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
    }


    public void TakeDamage(int amount)
    {
        if (hitTimer < 0)
        {
            hitTimer = 0.75f;
            currentHealth -= amount;
            PlayerPrefs.SetInt("currentHealth", currentHealth);
            GetComponent<PlayerAnimation>().isPain = true;
        }
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Dead();
        }

        healthBar.sizeDelta = new Vector2(currentHealth, healthBar.sizeDelta.y);
    }

    void Dead()
    {
        gameObject.GetComponent<PlayerAnimation>().isDead = true;
        isDead = true;
    }
}


