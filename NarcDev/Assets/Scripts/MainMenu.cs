using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {
    public GUIStyle Label;
    public GUIStyle Button;
    public GameObject loadingImage;



    void Start()
    {
        
    }
    private void NewGame(int level)
    {
        loadingImage.SetActive(true);
        SceneManager.LoadScene(level);

    }
   
    /*void Awake () {

        DontDestroyOnLoad(gameObject);

    }*/
    void OnGUI()
    {
        
        GUI.Label(new Rect(10,40,300,100), "Narc Division", Label);

        if(GUI.Button(new Rect(10,110,300,30),"New Game", Button))
        {
            Debug.Log("Click");
            NewGame(1);


        }
        if(GUI.Button(new Rect(10,150,300,30),"Load Game", Button))
        {
            //Load game code here
        }
        if(GUI.Button(new Rect(10,190,300,30),"Options", Button))
        {
            //Options game code here
        }
        if(GUI.Button(new Rect(10,230,300,30),"Quit", Button))
        {
            Debug.Log("Clack");
            Application.Quit();
        }
    }
}
