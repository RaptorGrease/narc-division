﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDrugs : MonoBehaviour
{
    public GameObject weedAnimation;
    public GameObject cocaineAnimation;
    public GameObject methAnimation;
    public GameObject shroomAnimation;

    Animator anim;
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    
    void Update()
    {

    }

    void TakeDrugs(DrugTypes type)
    {
        GameObject drugFX;
        switch (type)
        {
            default: // DrugTypes.weed
                drugFX = Instantiate(weedAnimation, transform.position, transform.rotation);
                drugFX.transform.parent = transform;
                drugFX.transform.position = new Vector2(0.1f, -0.7f);
                drugFX.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                break;
            case DrugTypes.cocaine:
                drugFX = Instantiate(cocaineAnimation, transform.position, transform.rotation);
                drugFX.transform.parent = transform;
                drugFX.transform.position = new Vector2(0.1f, -0.7f);
                drugFX.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                break;
            case DrugTypes.meth:
                drugFX = Instantiate(methAnimation, transform.position, transform.rotation);
                drugFX.transform.parent = transform;
                drugFX.transform.position = new Vector2(0.1f, -0.7f);
                drugFX.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                break;
            case DrugTypes.shrooms:
                drugFX = Instantiate(shroomAnimation, transform.position, transform.rotation);
                drugFX.transform.parent = transform;
                drugFX.transform.position = new Vector2(0, 0);
                drugFX.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
                break;
        }
        Destroy(drugFX, 6f);
    }
}
