﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDialog : MonoBehaviour
{

    //public string dialogue;
    private DialogueManager dM;
    private bool paused;
    public string[] dialogLines;
    private GameObject player;
    private float encounterRange = 1.5f;
    public bool encounterDone = false;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        dM = FindObjectOfType<DialogueManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!dM)
        {
            dM = FindObjectOfType<DialogueManager>();
        }
        if (Vector2.Distance(transform.position, player.transform.position) <= encounterRange && !encounterDone)
        {
                Time.timeScale = 0;
            if (!dM.dialogActive)
            {
                dM.dialogLines = dialogLines;
                dM.currentLine = 0;
                dM.ShowDialog();
                
            }

            encounterDone = true;
        }

    }
}

