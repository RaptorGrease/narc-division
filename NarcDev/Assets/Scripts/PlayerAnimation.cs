﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public enum Dir { SouthWest = 1, South, SouthEast, West, Center, East, NorthWest, North, NorthEast };

public class PlayerAnimation : MonoBehaviour
{
    public GameObject legs;
    public GameObject healingEffect;
    public GameObject piriToimii;
    public float speed = 0.01f;
    public int shootDelay = 15;
    public bool isPain = false;
    public bool isDead = false;
    public float coeffSpeedUp;
    public bool revive = false;
    public bool isDown = false;
    public Vector3 spawnLoc;
    public float angle;
    public bool isShoot = false;
    public Dir dir = Dir.Center;
    int shotsLeft;
    Animator _anim;
    int _shoot = 0;
    bool _doneDying = false;
    float _buffTimer;
    private float chillTimer = 2f;
    private bool chillOut = false;
    GunManager gunMan;

    void Start()
    {
        spawnLoc = transform.position;
        gunMan = GetComponent<GunManager>();
        _anim = GetComponent<Animator>();
        legs.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder;
    }

    // used to check which animation to play
    void Update()
    {

        if (revive)
        {
            gameObject.GetComponent<Health>().currentHealth = 100;
            _doneDying = false;
            isDead = false;
            _anim.SetTrigger("revive");
            revive = false;
            legs.GetComponent<SpriteRenderer>().enabled = true;
            PlayerPrefs.SetInt("currentHealth", 100);
        }

        if (_doneDying)
        {
            return;
        }

        legs.GetComponent<SpriteRenderer>().enabled = !isPain;
        if (isDead)
        {
            _anim.SetTrigger("dead");
            _doneDying = true;
            legs.GetComponent<SpriteRenderer>().enabled = false;
        }

        _anim.SetFloat("horizontal", Input.GetAxisRaw("Horizontal"));
        _anim.SetFloat("vertical", Input.GetAxisRaw("Vertical"));
        _anim.SetBool("fire1", Input.GetButton("Fire1"));

        Vector3 mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        angle = Mathf.Atan2(mouse.y - transform.position.y, mouse.x - transform.position.x) * 180 / Mathf.PI;
        Dir _dir = Dir.Center;
        _dir = (angle > 157.50f && angle < 180.00f) ? Dir.West : _dir;
        _dir = (angle > 112.50f && angle < 157.50f) ? Dir.NorthWest : _dir;
        _dir = (angle > 67.500f && angle < 112.50f) ? Dir.North : _dir;
        _dir = (angle > 22.500f && angle < 67.500f) ? Dir.NorthEast : _dir;
        _dir = (angle > -22.50f && angle < 22.500f) ? Dir.East : _dir;
        _dir = (angle > -67.50f && angle < -22.50f) ? Dir.SouthEast : _dir;
        _dir = (angle > -112.5f && angle < -67.50f) ? Dir.South : _dir;
        _dir = (angle > -157.5f && angle < -112.5f) ? Dir.SouthWest : _dir;
        _dir = (angle > -180.0f && angle < -157.5f) ? Dir.West : _dir;
   

        if (dir != _dir)
        {
            if (Time.timeScale == 1)
            {
                dir = _dir;
                _anim.SetInteger("direction", (int)_dir);
                _anim.SetBool("isTurn", true);
            }
    
        }
        else
        {
            _anim.SetBool("isTurn", false);
        }

        _anim.SetBool("isPain", isPain);
    }


    void OnTriggerEnter2D(Collider2D other)
    {

        var get = GetComponent<Health>();

        if (other.gameObject.tag == "Health")
        {
            Destroy(other.gameObject);
            var healAnimation = Instantiate(healingEffect, transform.position, transform.rotation);
            healAnimation.transform.parent = gameObject.transform;
            healAnimation.transform.localPosition = new Vector2(0.05f, -0.5f);
            healAnimation.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
            Destroy(healAnimation, 2.9f);
            get.currentHealth += 20;
            if (get.currentHealth >= 100)
            {
                get.currentHealth = 100;
            }
            get.healthBar.sizeDelta = new Vector2(get.currentHealth, get.healthBar.sizeDelta.y);
        }

        if (other.gameObject.tag == "Collectable")
        {
            Destroy(other.gameObject);
            var piriAnimation = Instantiate(piriToimii, transform.position, transform.rotation);
            piriAnimation.transform.parent = gameObject.transform;
            piriAnimation.transform.localPosition = new Vector2(0.05f, -0.5f);
            piriAnimation.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
            Destroy(piriAnimation, 5.8f);
            coeffSpeedUp = 1.5f;
            _buffTimer = 5.8f;
        }

    }

    // for getting player input
    void FixedUpdate()
    {
        if (chillOut)
        {
            chillTimer -= Time.deltaTime;
            if (chillTimer < 0)
            {
                speed = 0.03f;
                chillTimer = 2f;
                chillOut = false;
            }
        }
        if (!isPain && !_doneDying)
        {
            DoMove();
            DoShoot();
        }
        else
        {
            _anim.SetBool("isWalk", false);
        }

        isPain = Input.GetButton("Fire2");
    }

    private void DoMove()
    {
        _buffTimer -= Time.deltaTime;

        if (_buffTimer < 0)
        {
            coeffSpeedUp = 1f;
        }

        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        _anim.SetBool("isWalk", (moveX != 0f || moveY != 0f));
        transform.Translate(new Vector3(moveX, moveY, 0).normalized * speed * coeffSpeedUp, Space.World);
    }
    void ChillOut()
    {

        speed *= 0.75f;
        if (speed < 0.03f)
        {
            chillOut = true;
        }
    }
    private void DoShoot()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetButton("Fire1"))
            {
                spawnLoc = transform.position;
                isDown = false;
                switch (dir)
                {
                    case Dir.SouthWest:
                        spawnLoc.x -= 0.4f;
                        break;
                    case Dir.South:
                        isDown = true;
                        spawnLoc.x -= 0.1f;
                        break;
                    case Dir.SouthEast:
                        spawnLoc.x += 0.2f;
                        break;
                    case Dir.West:
                        spawnLoc.x -= 0.4f;
                        break;
                    case Dir.East:
                        spawnLoc.x += 0.4f;
                        break;
                    case Dir.NorthWest:
                        spawnLoc.x -= 0.2f;
                        break;
                    case Dir.North:
                        spawnLoc.y += 0.1f;
                        break;
                    case Dir.NorthEast:
                        spawnLoc.x += 0.3f;
                        break;
                }
                gunMan.Shoot();
            }

            if (_shoot > 0)
            {
                _shoot--;
            }
            else
            {
                isShoot = false;
                _shoot = 10;
            }

            _anim.SetBool("isShoot", isShoot);
        }
    }
}