﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChillingTrigger : MonoBehaviour {

    private PlayerAnimation player;
	void Start () {
        player = GetComponent<PlayerAnimation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            player.speed *= 0.75f;   
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            player.speed *= 1f;   
        }
    }
}
