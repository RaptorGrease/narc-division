﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{
    Transform player;
    bool lead = false;

    void Start()
    {
        // find the Player object's transform
        player = GameObject.FindGameObjectWithTag("Player").transform;

        // make sure we have a BoxCollider2D
        if (!gameObject.GetComponent<BoxCollider2D>())
        {
            gameObject.AddComponent<BoxCollider2D>();
            gameObject.GetComponent<BoxCollider2D>().size = new Vector2(2.6f, 1.8f);
            gameObject.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }

    void Update()
    {
        // should the camera take the lead?
        if (lead)
        {
            Dir dir = player.gameObject.GetComponent<PlayerAnimation>().dir;
            Vector2 angle = new Vector2(0, 0);

            // define vector values based on calculated mouse location from PlayerAnimation
            switch (dir)
            {
                case Dir.SouthWest:
                    angle.Set(-1, -1);
                    break;
                case Dir.South:
                    angle.Set(0, -1);
                    break;
                case Dir.SouthEast:
                    angle.Set(1, -1);
                    break;
                case Dir.West:
                    angle.Set(-1, 0);
                    break;
                case Dir.East:
                    angle.Set(1, 0);
                    break;
                case Dir.NorthWest:
                    angle.Set(-1, 1);
                    break;
                case Dir.North:
                    angle.Set(0, 1);
                    break;
                case Dir.NorthEast:
                    angle.Set(1, 1);
                    break;
            }
            // make it an unit vector
            angle.Normalize();

            // quickly, move the camera a bit ahead in the diection the Player is looking
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.position.x + (angle.x * 1.5f), player.position.y + (angle.y * 1.5f), -10), (Time.deltaTime * 2f));

            // has Player stopped moving?
            if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
            {
                lead = false;
            }
        }
        else
        {
            // calmly, move the camera to the Player's location
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.position.x, player.position.y, -10), Time.deltaTime);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // has the Player left the Comfort Zone?
        if (collision.gameObject.CompareTag("Player"))
        {
            lead = true;
        }
    }
}
