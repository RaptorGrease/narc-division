using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    public int damage = 10;
    void OnTriggerEnter2D(Collider2D collision)
    {


        /*var hit2 = collision.gameObject;
		var health2 = hit2.GetComponent<EnemyHealth> ();*/


        if (collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            collision.gameObject.GetComponent<Health>().TakeDamage(damage);
            
        }
     
    }
    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = transform.up * 2.5f;
    }
}