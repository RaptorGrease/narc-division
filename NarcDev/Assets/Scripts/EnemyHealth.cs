﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{

    public int maxHealth = 30;
    public int currentHealth;
    public float dropRate = 1f;
    public GameObject[] loot;
    private Animator _anim;
    private bool _isDead = false;

    void Start()
    {
        currentHealth = maxHealth;
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (currentHealth <= 0 && !_isDead)
        {
            _isDead = true;
            RandomDrop();
            Dead();
        }
    }

    void Dead()
    {
        _anim.SetBool("isTurn", false);
        _anim.SetTrigger("dead");
        Destroy(gameObject, 1f);
    }

    void RandomDrop()
    {
        if (dropRate > Random.Range(0.0f, 1.0f))
        {
            GameObject pickupdrop = Instantiate(loot[Random.Range(0, loot.Length)], gameObject.transform.position, Quaternion.identity);
            pickupdrop.GetComponent<SpriteRenderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder + 1;
        }
    }
}
