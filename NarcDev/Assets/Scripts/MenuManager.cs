﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private Button accept;
    private Button reject;


    void Start()
    {
        GameObject controls = GameObject.Find("Controls");
        foreach (Transform item in transform)
        {
            if(item.gameObject.name == "Controls")
            {
                controls = item.gameObject;
            }
        }
        foreach (Transform item in controls.transform)
        {
            if (item.gameObject.name == "Accept")
            {
                accept = item.gameObject.GetComponent<Button>();
            }
            if (item.gameObject.name == "Reject")
            {
                reject = item.gameObject.GetComponent<Button>();
            }
        }
        accept.onClick.AddListener(AcceptAction);
        reject.onClick.AddListener(RejectAction);
    }
    public void NewGame(int level)
    {
        PlayerPrefs.SetInt("startHealth",100);
        PlayerPrefs.SetInt("currentHealth",100);
        SceneManager.LoadScene(level);


    }
    // Save Changes
    private void AcceptAction()
    {
        throw new NotImplementedException("Accept button needs functionality!");
    }

    // Reject Changes
    private void RejectAction()
    {
        throw new NotImplementedException("Reject button needs functionality!");
    }
}
