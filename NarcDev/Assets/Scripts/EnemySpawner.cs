﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

[ExecuteInEditMode]
public class EnemySpawner : MonoBehaviour
{
    public Vector2 dangerZone = new Vector2(5, 5);
    public int currentEnemies = 0;
    public int totalEnemies = 100;
    public int enemyLimit = 10;
    public int reinforcements;
    public bool mustClear = false;
    public bool showTime = false;

    float[,] zonePoints;
    LineRenderer lineRend;
    PolygonCollider2D polyCol;
    EdgeCollider2D edgeCol;
    EnemyManager em;

    void Start()
    {
        em = FindObjectOfType<EnemyManager>();
        reinforcements = totalEnemies;
        lineRend = GetComponent<LineRenderer>();
        zonePoints = MakePointArray();
        polyCol = GetComponent<PolygonCollider2D>();
        polyCol.SetPath(0, MakeZoneArray(0.9f));
        edgeCol = GetComponent<EdgeCollider2D>();
        edgeCol.points = MakeZoneArray(1f);
        edgeCol.enabled = false;

        if (Application.isEditor)
        {
            GetComponent<SpriteRenderer>().enabled = true;
            lineRend.enabled = true;
        }

        if (Application.isPlaying)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            lineRend.enabled = false;
        }
    }

    void Update()
    {
        if (Application.isEditor)
        {
            zonePoints = MakePointArray();
            lineRend.SetPositions(MakeZoneLine());
            polyCol.SetPath(0, MakeZoneArray(0.9f));
        }

        if (Application.isPlaying)
        {
            currentEnemies = gameObject.GetComponentsInChildren<EnemyAI>().Length;
            if (reinforcements > 0 && currentEnemies < enemyLimit)
            {
                GameObject baddie = Instantiate(em.enemyPrefabs[Random.Range(0, em.enemyPrefabs.Length)]);
                baddie.transform.SetParent(transform);
                baddie.transform.localPosition = new Vector3(Random.Range(-(dangerZone.x * 0.4f), (dangerZone.x * 0.4f)), Random.Range(-(dangerZone.y * 0.4f), (dangerZone.y * 0.4f)));
                em.SendMessage("EnemyListChanged");
                reinforcements--;
            }

            if (showTime)
            {
                showTime = (reinforcements > 0 || currentEnemies > 0);
                edgeCol.enabled = showTime;
            }
        }

        GetComponent<SpriteRenderer>().color = new Color((mustClear) ? 0f : 1f, 1f, 1f);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (mustClear)
            {
                showTime = true;
            }
        }
    }

    float[,] MakePointArray()
    {
        return new float[,] {
            { dangerZone.x * 0f, dangerZone.y * 0.5f },
            { dangerZone.x * 0.35f, dangerZone.y * 0.35f },
            { dangerZone.x * 0.5f, dangerZone.y * 0f },
            { dangerZone.x * 0.35f, -dangerZone.y * 0.35f },
            { dangerZone.x * 0f, -dangerZone.y * 0.5f },
            { -dangerZone.x * 0.35f, -dangerZone.y * 0.35f },
            { -dangerZone.x * 0.5f, dangerZone.y * 0f },
            { -dangerZone.x * 0.35f, dangerZone.y * 0.35f }
        };
    }

    Vector2[] MakeZoneArray(float scale)
    {
        return new Vector2[] {
            new Vector2(zonePoints[0,0], zonePoints[0,1]) * scale,
            new Vector2(zonePoints[1,0], zonePoints[1,1]) * scale,
            new Vector2(zonePoints[2,0], zonePoints[2,1]) * scale,
            new Vector2(zonePoints[3,0], zonePoints[3,1]) * scale,
            new Vector2(zonePoints[4,0], zonePoints[4,1]) * scale,
            new Vector2(zonePoints[5,0], zonePoints[5,1]) * scale,
            new Vector2(zonePoints[6,0], zonePoints[6,1]) * scale,
            new Vector2(zonePoints[7,0], zonePoints[7,1]) * scale,
            new Vector2(zonePoints[0,0], zonePoints[0,1]) * scale
        };
    }

    Vector3[] MakeZoneLine()
    {
        return new Vector3[] {
            new Vector3(zonePoints[0,0], zonePoints[0,1], -9f),
            new Vector3(zonePoints[1,0], zonePoints[1,1], -9f),
            new Vector3(zonePoints[2,0], zonePoints[2,1], -9f),
            new Vector3(zonePoints[3,0], zonePoints[3,1], -9f),
            new Vector3(zonePoints[4,0], zonePoints[4,1], -9f),
            new Vector3(zonePoints[5,0], zonePoints[5,1], -9f),
            new Vector3(zonePoints[6,0], zonePoints[6,1], -9f),
            new Vector3(zonePoints[7,0], zonePoints[7,1], -9f)
        };
    }
}
