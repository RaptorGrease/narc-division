﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExplosion : MonoBehaviour
{
    public ExplosionTypes type;
    public Color filter;
    public bool complete = false;

    void Awake()
    {
        complete = false;
        enabled = false;
    }

    void Start()
    {
        GetComponent<Animator>().Play(string.Format("explosion-{0}", (int)type));
        GetComponent<SpriteRenderer>().color = filter;
    }

    private void FixedUpdate()
    {
        if(complete)
        {
            Destroy(gameObject);
        }
    }
}
