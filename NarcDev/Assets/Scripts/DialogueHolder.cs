using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueHolder : MonoBehaviour {

    //public string dialogue;
    private DialogueManager dM;
    private bool paused;
	public string[] dialogLines;
	void Start () {
       
        dM = FindObjectOfType<DialogueManager>();
	}
	
	// Update is called once per frame
	void Update () {
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (!dM)
        {
            dM = FindObjectOfType<DialogueManager>();
        }
        if (other.gameObject.tag == "Player")
        {
            if (Input.GetMouseButtonDown(0))
            {
               // dM.ShowBox(dialogue);
                Time.timeScale = 0;
				if (!dM.dialogActive) {
					dM.dialogLines = dialogLines;
					dM.currentLine = 0;
					dM.ShowDialog();
				}
            }

        }
    }
}
